# Instalación de Openstack

Esto es una guía para instalar un entorno de Openstack que se asemeja al siguiente esquema:

![Esquema de Openstack](/esquema.png)

La guía está pensada para un entorno de alta disponibilidad y para ello iremos paso a paso en nueve apartados