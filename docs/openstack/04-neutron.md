# 4. Neutron

## 4.1 Instalación

### 4.1.1 Controladores

**controllers**

Instalamos los paquetes para Neutron:

```shell
sudo apt install neutron-server
```

### 4.1.2 Networking

**gateways**

Instalamos los paquetes para Neutron:

```sh
sudo apt install neutron-plugin-ml2 neutron-openvswitch-agent neutron-l3-agent neutron-dhcp-agent neutron-metadata-agent
```

### 4.1.3 Computación

**kvm01 kvm02**

Instalamos los paquetes para Neutron:

```sh
sudo apt install neutron-openvswitch-agent
```

## 4.2 Base de datos

**controller01**

Creamos la base de datos:

```sql
sudo mysql

CREATE DATABASE neutron;
GRANT ALL PRIVILEGES ON neutron.* TO 'neutronuser'@'localhost' IDENTIFIED BY 'neutronpass';
GRANT ALL PRIVILEGES ON neutron.* TO 'neutronuser'@'%' IDENTIFIED BY 'neutronpass';
```

## 4.3 Servicio, usuario y endpoints

**controller01**

Creamos el usuario `neutron` y lo añadimos al proyecto `service` con el rol `admin`:

```sh
source adminrc

openstack user create --domain default --password n37tr0n neutron

openstack role add --project service --user neutron admin
```

Creamos el servicio `neutron`:

```sh
openstack service create --name neutron --description "OpenStack Networking" network
```

Creamos los endpoints para Neutron:

```sh
openstack endpoint create --region SanSebastian network public http://public-api.mydomain.local:9696
openstack endpoint create --region SanSebastian network internal http://controller:8778
openstack endpoint create --region SanSebastian network admin http://controller:8778
```

## 4.4 Configuración

### 4.4.1 Controladores

**controllers**

Configuramos Neutron editando el fichero `/etc/neutron/neutron.conf`:

```ini
[DEFAULT]
bind_host = controller0x
bind_port = 9696
transport_url = rabbit://openstackuser:openstackpass@controller01:5672,openstackuser:openstackpass@controller02:5672/openstack
auth_strategy = keystone
core_plugin = ml2
service_plugins = router
allow_overlapping_ips = true
notify_nova_on_port_status_changes = true
notify_nova_on_port_data_changes = true

[database]
#connection = sqlite:////var/lib/neutron/neutron.sqlite
connection = mysql+pymysql://neutronuser:neutronpass@controller/neutron

[keystone_authtoken]
auth_uri = http://public-api.mydomain.local:5000
auth_url = http://controller:35357
auth_type = password
project_domain_name = default
user_domain_name = default
project_name = service
username = neutron
password = n37tr0n

backend = dogpile.cache.memcached
memcache_servers = controller01:11211, controller02:11211
memcache_dead_retry = 300
memcache_socket_timeout = 3

[nova]
auth_url = http://controller:35357
auth_type = password
project_domain_name = default
user_domain_name = default
region_name = SanSebastian
project_name = service
username = nova
password = n0v4
```

Configuramos el plugin ML2 editando el fichero `/etc/neutron/plugins/ml2/ml2_conf.ini`:

```ini
[ml2]
type_drivers = flat,vxlan
tenant_network_types = vxlan
mechanism_drivers = openvswitch,l2population
extension_drivers = port_security

[ml2_type_flat]
flat_networks = provider

[ml2_type_vxlan]
vni_ranges = 1:1000

[securitygroup]
firewall_driver = neutron.agent.linux.iptables_firewall.OVSHybridIptablesFirewallDriver
enable_ipset = true
```

Configuramos Nova para que use Neutron editando el fichero `/etc/nova/nova.conf`:

```ini
[neutron]
url = http://controller:9696
auth_url = http://controller:35357
auth_type = password
project_domain_name = default
user_domain_name = default
region_name = SanSebastian
project_name = service
username = neutron
password = n37tr0n
service_metadata_proxy = true

metadata_proxy_shared_secret = 1234567890
```

Reiniciamos los servicios:

```sh
sudo service neutron-server restart
sudo service nova-api restart
```

### 4.4.2 Networking

**gateways**

En el fichero `/etc/sysctl.conf` añadimos:

```ini
net.ipv4.ip_forward=1
net.ipv4.conf.all.rp_filter=0
net.ipv4.conf.default.rp_filter=0
```

Aplicamos los parámetros:

```shell
sudo sysctl -p
```

Configuramos Neutron editando el fichero `/etc/neutron/neutron.conf`:

```ini
[DEFAULT]
transport_url = rabbit://openstackuser:openstackpass@controller01:5672,openstackuser:openstackpass@controller02:5672/openstack
auth_strategy = keystone
core_plugin = ml2
service_plugins = router
allow_overlapping_ips = true

[keystone_authtoken]
auth_uri = http://public-api.mydomain.local:5000
auth_url = http://controller:35357
auth_type = password
project_domain_name = default
user_domain_name = default
project_name = service
username = neutron
password = n37tr0n
```

Configuramos el plugin ML2 editando el fichero `/etc/neutron/plugins/ml2/ml2_conf.ini`:

```ini
[ml2]
type_drivers = flat,vxlan
tenant_network_types = vxlan
mechanism_drivers = openvswitch,l2population
extension_drivers = port_security

[ml2_type_flat]
flat_networks = provider

[ml2_type_vxlan]
vni_ranges = 1:1000

[securitygroup]
firewall_driver = neutron.agent.linux.iptables_firewall.OVSHybridIptablesFirewallDriver
enable_ipset = true
```

Configuramos el agente OpenVSwitch editando el fichero `/etc/neutron/plugins/ml2/openvswitch_agent.ini`:

```ini
[ovs]
local_ip = 172.16.200.5x
bridge_mappings = provider:br-ex

[agent]
tunnel_types = vxlan

[securitygroup]
firewall_driver = neutron.agent.linux.iptables_firewall.OVSHybridIptablesFirewallDriver
enable_ipset = true
```

Cambiamos la configuración de la interfaz `eno2`  (o como se llame) editando el fichero `/etc/network/interfaces`:

```shell
#auto eno2
#iface ens4 inet dhcp

auto eno2
iface eno2 inet manual
  up ip address add 0/0 dev $IFACE
  up ip link set $IFACE up
  up ip link set $IFACE promisc on
  down ip link set $IFACE promisc
```

Creamos los bridges y añadimos la interfaz `eno2` (enp9s0f0, eno1, como se llame) al bridge `br-ex`:

```shell
sudo ovs-vsctl add-br br-int #por defecto se suele crear en ubuntu al menos
sudo ovs-vsctl add-br br-ex
sudo ovs-vsctl add-port br-ex eno2
sudo ovs-vsctl show  #para verificar los bridges
```

Configuramos el agente L3 editando el fichero `/etc/neutron/l3_agent.ini`:

```ini
[DEFAULT]
interface_driver = openvswitch
```

Configuramos el agente DHCP editando el fichero `/etc/neutron/dhcp_agent.ini`:

```ini
[DEFAULT]
interface_driver = openvswitch
ovs_integration_bridge = br-int
dhcp_driver = neutron.agent.linux.dhcp.Dnsmasq
enable_isolated_metadata = true
```

Configuramos el agente Metadata editando el fichero `/etc/neutron/metadata_agent.ini`:

```ini
[DEFAULT]
nova_metadata_host = controller
nova_metadata_port = 8775
metadata_proxy_shared_secret = 1234567890
```

**controller01**

Poblamos la base de datos:

```shell
sudo su -s /bin/sh -c "neutron-db-manage --config-file /etc/neutron/neutron.conf --config-file /etc/neutron/plugins/ml2/ml2_conf.ini upgrade head" neutron
```

**NOTA:** Esto hay que hacerlo con todos los plugins configurados

**gateways**

Reiniciamos los servicios:

```shell
sudo service neutron-openvswitch-agent restart
sudo service neutron-l3-agent restart
sudo service neutron-dhcp-agent restart
sudo service neutron-metadata-agent restart
```

### 4.4.3 Computación

**kvm01 kvm02**

En el fichero `/etc/sysctl.conf` añadimos:

```ini
net.ipv4.conf.all.rp_filter=0
net.ipv4.conf.default.rp_filter=0
```

Aplicamos los parámetros:

```shell
sudo sysctl -p
```

Configuramos Neutron editando el fichero `/etc/neutron/neutron.conf`:

```ini
[DEFAULT]
transport_url = rabbit://openstackuser:openstackpass@controller01:5672,openstackuser:openstackpass@controller02:5672/openstack
auth_strategy = keystone

[keystone_authtoken]
auth_uri = http://public-api.mydomain.local:5000
auth_url = http://controller:35357
auth_type = password
project_domain_name = default
user_domain_name = default
project_name = service
username = neutron
password = n37tr0n
```

Configuramos el agente OpenVSwitch editando el fichero `/etc/neutron/plugins/ml2/openvswitch_agent.ini`:

```ini
[ovs]
local_ip = 172.16.200.10x

[agent]
tunnel_types = vxlan

[securitygroup]
firewall_driver = neutron.agent.linux.iptables_firewall.OVSHybridIptablesFirewallDriver
enable_ipset = true
```

**NOTA:** Nos aseguramos que hemos cambiado la `x` por `1` en caso de `kvm01` y `2` en caso de `kvm02`

Configuramos Nova para que use Neutron editando el fichero `/etc/nova/nova.conf`:

```ini
[neutron]
url = http://controller:9696
auth_url = http://controller:35357
auth_type = password
project_domain_name = default
user_domain_name = default
region_name = SanSebastian
project_name = service
username = neutron
password = n37tr0n
```

Reiniciamos los servicios:

```ini
sudo service nova-compute restart
sudo service neutron-openvswitch-agent restart
```

## 4.5 Comprobaciones

**controler01**

Comprobamos los agentes:

```shell
source adminrc

openstack network agent list
```

Comprobamos las redes y routers:

```shell
source hvsistemasrc

openstack network list
openstack router list
```

## 4.6 Debugging

**Gateways**

Para poder hacer las pruebas desde "dentro" de una red de OpenStack, podemos hacer lo siguiente:

1. Listar routers y servidores dhcp: `ip netns list`
2. ping: `ip netns exec dhcpID  ping IP`
3. arp: `ip netns exec routerID arp -an`
4. entrar en un router: `ip netns exec routerID bash` 

**KVMs**

Si las IP's flotantes se quedan "colgadas" probablemente haya que reiniciar el servicio de openvswitch en los KVM:

```shell
service openvswitch-switch restart
```

## 4.7 HA

- [Explicación de VRRP y DVR](https://assafmuller.com/2014/08/16/layer-3-high-availability/)
- [Más info aquí](https://docs.openstack.org/neutron/pike/admin/deploy-ovs-ha-vrrp.html)

Para que los servicios de red estén en alta disponibilidad hay que añadir lo siguiente en neutron.conf:

**Controllers**

`/etc/neutron/neutron.conf`

```ini
[DEFAULT]
l3_ha = true
##opcionales
max_l3_agents_per_router = 3
min_l3_agents_per_router = 2
```

Reiniciamos neutron-server

**Gateways**

En los Gateways en `/etc/neutron/l3_agent.ini`:

```ini
[DEFAULT]
interface_driver = openvswitch
external_network_bridge =
```

En `/etc/neutron/plugins/ml2/openvswitch_agent.ini`:

```ini
[ovs]
bridge_mappings = provider:br-ex
local_ip = 172.16.200.5x

[agent]
tunnel_types = vxlan
l2_population = true

[securitygroup]
firewall_driver = neutron.agent.linux.iptables_firewall.OVSHybridIptablesFirewallDriver
```

Y reiniciamos neutron-openvswitch-agent y neutron-l3-agent
