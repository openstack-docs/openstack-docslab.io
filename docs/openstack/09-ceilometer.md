# 9. CEILOMETER

## 9.1 Instalación

**KVM01 y KVM02**
Instalamos paquetes de Ceilometer:

```shell
sudo apt-get install ceilometer-agent-compute
```

Editamos el fichero `/etc/ceilometer/ceilometer.conf`:

```ini
[DEFAULT]
transport_url = rabbit://openstackuser:openstackpass@controller01:5672,openstackuser:openstackpass@controller02:5672/openstack

[keystone_authtoken]
auth_uri = http://controller:5000
auth_url = http://controller:35357
auth_type = password
project_domain_name = default
user_domain_name = default
project_name = service
username = ceilometer
password = c31l0m3t3r

backend = dogpile.cache.memcached
memcache_servers = controller01:11211, controller02:11211
memcache_dead_retry = 300
memcache_socket_timeout = 3

[service_credentials]
auth_url = http://controller:5000
auth_type = password
project_domain_id = default
user_domain_id = default
project_name = service
username = ceilometer
password = c31l0m3t3r
interface = internalURL

```

Editamos el fichero `/etc/nova/nova.conf`:

```ini
[DEFAULT]
...
instance_usage_audit = True
instance_usage_audit_period = hour
notify_on_state_change = vm_and_task_state

[oslo_messaging_notifications]
...
driver = messagingv2
```

Reiniciamos servicios:

```shell
sudo service ceilometer-agent-compute restart
sudo service nova-compute restart
```

**Controllers**
Usamos las credenciales de admin y creamos el usuario ceilometer y lo añadimos al proyecto service con el rol admin:

```shell
. adminrc

openstack user create --domain default --password c31l0m3t3r ceilometer

openstack role add --project service --user ceilometer admin
```

Creamos el servicio ceilometer:

```shell
openstack service create --name ceilometer --description "Telemetry" metering
```

Registramos a gnocchi en Keystone y añadimos creamos su servicio:

```shell
openstack user create --domain default --password gn0cch1 gnocchi

openstack role add --project service --user gnocchi admin

openstack service create --name gnocchi --description "Metric Service" metric
```

Creamos los endpoints para el servicio gnocchi:

```shell
openstack endpoint create --region SanSebastian metric public http://public-api.mydomain.local:8041

openstack endpoint create --region SanSebastian metric internal http://controller:8041

openstack endpoint create --region SanSebastian metric admin http://controller:8041
```

Instalamos los paquetes de Gnocchi:

```shell
sudo apt install gnocchi-api gnocchi-metricd python-gnocchiclient
```

(dpkg-reconfigure -plow gnocchi-common)

Creamos la base de datos y el usuario para gnocchi:

`sudo mysql -p`

```sql
CREATE DATABASE gnocchi;
GRANT ALL PRIVILEGES ON gnocchi.* TO 'gnocchi'@'localhost' IDENTIFIED BY 'gn0cch1';
GRANT ALL PRIVILEGES ON gnocchi.* TO 'gnocchi'@'%' IDENTIFIED BY 'gn0cch1';
```

Editamos el fichero `/etc/gnocchi/gnocchi.conf`:

```ini
[api]
auth_mode = keystone

[keystone_authtoken]
...
auth_type = password
auth_uri = http://public-api.mydomain.local:5000
auth_url = http://controller:35357
project_domain_name = Default
user_domain_name = Default
project_name = service
username = gnocchi
password = gn0cch1
interface = internalURL
region_name = SanSebastian

backend = dogpile.cache.memcached
memcache_servers = controller01:11211, controller02:11211
memcache_dead_retry = 300
memcache_socket_timeout = 3

[indexer]
url = mysql+pymysql://gnocchi:gn0cch1@controller/gnocchi

[storage]
coordination_url = redis://controller:6379
file_basepath = /var/lib/gnocchi
driver = file
```

Instalamos Redis:

```shell
sudo apt-get install redis-server
```

(También se puede instalar mongo como bbdd: https://docs.openstack.org/mitaka/install-guide-ubuntu/environment-nosql-database.html)

Inicializamos gnocchi y reiniciamos servicios:

```shell
sudo gnocchi-upgrade
##############sudo service gnocchi-api restart
sudo service gnocchi-metricd restart
sudo service apache2 restart
```

Instalamos componentes:

```shell
sudo apt install ceilometer-agent-notification ceilometer-agent-central
```

Editamos `/etc/ceilometer/ceilometer.conf`:

```ini
[DEFAULT]
transport_url = rabbit://openstackuser:openstackpass@controller01:5672,openstackuser:openstackpass@controller02:5672/openstack

[dispatcher_gnocchi]
# filter out Gnocchi-related activity meters (Swift driver)
filter_service_activity = False
# default metric storage archival policy
archive_policy = low

[service_credentials]
auth_url = http://controller:5000
auth_type = password
project_domain_id = default
user_domain_id = default
project_name = service
username = ceilometer
password = c31l0m3t3r
interface = internalURL

[keystone_authtoken]
auth_uri = http://public-api.mydomain.local:5000/v3
auth_url = http://controller:35357
auth_type = password
project_domain_name = default
user_domain_name = default
project_name = service
username = ceilometer
password = c31l0m3t3r

backend = dogpile.cache.memcached
memcache_servers = controller01:11211, controller02:11211
memcache_dead_retry = 300
memcache_socket_timeout = 3

[compute]
instance_discovery_method = libvirt_metadata
```

Añadir lo siguiente a `/etc/apache2/sites-enabled/gnocchi-api.conf`:

```apacheconf
<Directory />
    Options FollowSymLinks
    AllowOverride None
    <IfVersion >= 2.4>
        Require all granted
    </IfVersion>
    <IfVersion < 2.4>
        Order allow,deny
        Allow from all
    </IfVersion>
</Directory>
```

Creamos los recursos de Gnocchi y reiniciamos servicios:

```shell
service apache2 restart
sudo ceilometer-upgrade --skip-metering-database ### Este comando sólo en un Controller
sudo service ceilometer-agent-central restart
sudo service ceilometer-agent-notification restart
```

## 9.2 Configuración en servicios

**Storage**
Editamos `/etc/cinder/cinder.conf`:

```ini
[oslo_messaging_notifications]
...
driver = messagingv2
```

Probamos que las estadísiticas periódicas funcionen (rellenando las horas y fechas):

```shell
cinder-volume-usage-audit  --start_time='YYYY-MM-DD HH:MM:SS' \
  --end_time='YYYY-MM-DD HH:MM:SS' --send_actions
```

Añadimos esto al cron `/etc/cron.d/cinder-stats`:

```shell
*/5 * * * * /usr/bin/cinder-volume-usage-audit --send_actions
```

Reiniciamos servicios en el **Controller**: 

```shell
sudo service apache2 restart
sudo service cinder-scheduler restart
```

Y en **Storage**:

```shell
sudo service cinder-volume restart
```

** Glance - Controller**
Editamos `/etc/glance/glance-api.conf` y `/etc/glance/glance-registry.conf`:

```ini
[DEFAULT]
transport_url = rabbit://openstackuser:openstackpass@controller01:5672,openstackuser:openstackpass@controller02:5672/openstack

[oslo_messaging_notifications]
driver = messagingv2
```

Reiniciamos los servicios:

```shell
sudo service glance-registry restart
sudo service glance-api restart
```

** Heat - Controller**
Editamos `/etc/heat/heat.conf`:

```ini
[oslo_messaging_notifications]
driver = messagingv2
```

Reiniciamos servicios:

```shell
sudo service heat-api restart
sudo service heat-api-cfn restart
sudo service heat-engine restart
```

**Neutron - Controller**
Editamos `/etc/neutron/neutron.conf`:

```ini
[oslo_messaging_notifications]
driver = messagingv2
```

Reiniciamos el servicio:

```shell
sudo service neutron-server restart
```

## 9.3 - Comprobaciones

para comprobar que todo funciona hay que hacer lo siguiente:

```shell
. adminrc
export OS_AUTH_TYPE=password
gnocchi resource list  --type image
gnocchi resource show a6b387e1-4276-43db-b17a-e10f649d85a3
```

## Si queremos usar grafana

En los ficheros `keystone.conf` y `gnocchi.conf` añadimos lo siguiente:

```ini
[cors]
allowed_origin = http://ruta-a-grafana:puerto
```
