# 7 Cinder

Partimos de una instalacion de cinder con lvm [07-1-cinder+lvm.md](07-1-cinder+lvm.md)

....

## 7.4 Configuración

### 7.4.1 Controladores

**controllers**

Configuramos Cinder editando el fichero `/etc/cinder/cinder.conf`:

```ini
[DEFAULT]
bind_host = controller0x

rootwrap_config = /etc/cinder/rootwrap.conf
api_paste_confg = /etc/cinder/api-paste.ini
iscsi_helper = tgtadm
volume_name_template = volume-%s
#volume_group = cinder-volumes
verbose = True
auth_strategy = keystone
state_path = /var/lib/cinder
lock_path = /var/lock/cinder
volumes_dir = /var/lib/cinder/volumes

enabled_backends = ceph

scheduler_driver=cinder.scheduler.filter_scheduler.FilterScheduler

transport_url = rabbit://openstackuser:openstackpass@controller01:5672,openstackuser:openstackpass@controller02:5672/openstack

auth_host = controllerp


auth_strategy = keystone
my_ip = 172.16.100.11

[oslo_concurrency]
lock_path = /var/lib/cinder/tmp

[database]
connection = mysql+pymysql://cinderuser:cinderpass@controller/cinder

[keystone_authtoken]
auth_uri = http://controller:5000
auth_url = http://controller:35357
auth_type = password
project_domain_name = default
user_domain_name = default
project_name = service
username = cinder
password = c1nd3r

backend = dogpile.cache.memcached
memcached_servers = controller01:11211, controller02:11211
memcache_dead_retry = 300
memcache_socket_timeout = 3

[cinder]
catalog_info = volumev3:cinderv3:adminURL

[oslo_messaging_rabbit]
rabbit_retry_interval=1
rabbit_retry_backoff=2
rabbit_max_retries=0
rabbit_ha_queues=true

```

Configuramos Nova editando el fichero `/etc/nova/nova.conf`:

```ini
[cinder]
catalog_info = volumev3:cinderv3:adminURL
```

Reiniciamos los servicios:

```shell
sudo service nova-api restart

sudo service cinder-scheduler restart
sudo service apache2 restart
```

### 7.4.2 Almacenamiento

**storage01**

Configuramos Cinder editando el fichero `/etc/cinder/cinder.conf`:

```ini
[DEFAULT]
rootwrap_config = /etc/cinder/rootwrap.conf
api_paste_confg = /etc/cinder/api-paste.ini
iscsi_helper = tgtadm
volume_name_template = volume-%s
#verbose = True

my_ip = 172.16.100.151
glance_catalog_info = image:glance:adminURL
transport_url = rabbit://openstackuser:openstackpass@controller01:5672,openstackuser:openstackpass@controller02:5672/openstack
auth_strategy = keystone
state_path = /var/lib/cinder
lock_path = /var/lock/cinder
volumes_dir = /var/lib/cinder/volumes

enabled_backends = ceph
#glance_api_version = 2

[ceph]
volume_backend_name=ceph
rbd_pool=volumes
rbd_user=volumes
rbd_secret_uuid=AQDDiP5aFh3uLxAA0We9NNu0Z7ZJZ+VUlcB+MA==
#rbd_secret_uuid=AQDBi/1a3YZyLBAA5Jn63AbwJjXjDov0TvJBMg==
volume_driver=cinder.volume.drivers.rbd.RBDDriver
rbd_ceph_conf=/etc/ceph/ceph.conf

backup_driver = cinder.backup.drivers.ceph

[oslo_concurrency]
lock_path = /var/lib/cinder/tmp

[oslo_messaging_notifications]
driver = messagingv2

[database]
connection = mysql+pymysql://cinderuser:cinderpass@controller/cinder

[keystone_authtoken]
auth_uri = http://controller:5000
auth_url = http://controller:35357
auth_type = password
project_domain_name = default
user_domain_name = default
project_name = service
username = cinder
password = c1nd3r
```

Reiniciamos los servicios:

```shell
sudo service tgt restart
sudo service cinder-volume restart
```

### 7.4.3 Computación

**kvm01 kvm02**

Configuramos Nova editando el fichero `/etc/nova/nova.conf`:
```ini
[cinder]
## hemos puesto el v3 porque no hemos configurado la v2
catalog_info = volumev3:cinderv3:adminURL
```

Reiniciamos el servicio:

```shell
sudo service nova-compute restart
```

**NOTA:** Esto es necesario para iniciar instancias desde volúmenes

## 7.5 Comprobación

### 7.5.1 Administrador

**controller01**

Añadimos la siguiente línea al fichero `/home/hvsistemas/adminrc`:

```shell
export OS_VOLUME_API_VERSION=3
```

Comprobamos que están todos los servicios:

```shell
source adminrc

openstack volume service list
```

### 7.5.2 Usuario

**local**

Añadimos la siguiente línea al fichero `/home/hvsistemas/hvsistemasrc`:

```shell
export OS_VOLUME_API_VERSION=3
```

Creamos un volumen:

```shell
source hvsistemasrc

openstack volume create vol01 --size 1
```

Listamos los volúmenes:

```shell
openstack volume list
```

Borramos el volumen:

```shell
openstack volume delete vol01
```

## 7.6 Operaciones

**Admin (Horizon)**
En `Volume Types` creamos los tipos de almacenamiento por cada backend que tengamos, por ejemplo:

```
Disco_Rapido01 --> volume_backend_name --> lvmFast01
Disco_Rapido02 --> volume_backend_name --> lvmFast02
Disco_Lento01 --> volume_backend_name --> lvmSlow01
```

**Para volúmenes "rebeldes"**
si al hacer un `lvremove nombredelvolumen` nos dice que está en uso:

```shell
service tgt stop
lvremove grupodelvolumen/nombredelvolumen
service tgt start
```

En el anterior metodo al parar el servicio tgt paramos la exportacion de volumenes de Cinder. Con el siguiente metodo no paramos el servicio tgt

- Sacamos el ID de los volumenes que dan problemas, en un controler:

```shell
source adminrc
openstack volume list --project ionar
+--------------------------------------+--------------------+----------------+------+-------------+
| ID                                   | Name               | Status         | Size | Attached to |
+--------------------------------------+--------------------+----------------+------+-------------+
| b6887e4f-ba21-4d4e-98b2-6dbfb0404259 | ceph11-node1-disk4 | error_deleting |  100 |             |
| 66609490-98be-42f4-b374-bf3d3f1483dc | ceph11-node1-disk2 | error_deleting |   50 |             |
| ce290bf8-e6e8-4471-8b5c-d18ac10cdd99 | ceph10-node0-disk2 | error_deleting |   50 |             |
+--------------------------------------+--------------------+----------------+------+-------------+
```

- Nos vamos a la maquina de Cinder y ejecutamos los siguientes comandos

```shell
dmsetup info -c | grep b6887e4f
cinderv1--Slow-volume--b6887e4f--ba21--4d4e--98b2--6dbfb0404259                                     252  30 L--w    1    1      0 LVM-n3x3zJi7RjmVQncLa1Kz11VeRZwlFGVRs5vvGXmlk3bHBZwPK9g6pelGjSdj88WU

ls -la /sys/dev/block/252\:30/holders/
drwxr-xr-x 2 root root 0 jul 19 09:57 .
drwxr-xr-x 9 root root 0 may  3 15:20 ..
lrwxrwxrwx 1 root root 0 jul 19 09:46 dm-38 -> ../../dm-38

dmsetup remove /dev/dm-38
```

- Nos volemos a uno de los controlers

```shell
openstack volume delete b6887e4f-ba21-4d4e-98b2-6dbfb0404259 --force
```

- Si listamos otra vez los volumenes tendremos que ver el volumen con error ya no esta:

```shell
openstack volume list --project ionar
+--------------------------------------+--------------------+----------------+------+-------------+
| ID                                   | Name               | Status         | Size | Attached to |
+--------------------------------------+--------------------+----------------+------+-------------+
| 66609490-98be-42f4-b374-bf3d3f1483dc | ceph11-node1-disk2 | error_deleting |   50 |             |
| ce290bf8-e6e8-4471-8b5c-d18ac10cdd99 | ceph10-node0-disk2 | error_deleting |   50 |             |
+--------------------------------------+--------------------+----------------+------+-------------+

```

**horizon (hvsistemas)**

- Creamos un volumen vacío `vol01` de 10GB
- Hacemos el __attach__ del volumen a una instancia
  - Formateamos
  - Montamos en `/mnt`
  - Creamos algún contenido
- Hacemos el __detach__ de esta instancia
  - ¿Por qué nos pregunta lo que nos pregunta?
  - ¿Podemos hacerlel __detach__ sin más?
- Hacemos el __attach__ a otra instancia
  - Comprobamos que los datos siguen ahí
- Hacemos el __detach__ de esta instancia
  - Ampliamos el volumen a 15GB
  - Volvemos a hacer el __attach__ a una instancia
  - Comprobamos el tamaño del volumen
- Hacemos un snapshot de vol01 de nombre `vol01-snap`
  - ¿Por qué nos pregunta lo que nos pregunta?
- Creamos un volumen desde el snapshot `vol01-snap` de nombre `vol02`
- Hacemos el __attach__ de `vol02` a una instancia
  - Comprobamos que los datos siguen ahí
  - Comprobamos el tamaño
- Hacemos el __detach__ de `vol02` y lo borramos
- Hacemos el __detach__ de `vol01` y lo borramos
  - ¿Por qué da error?

- Se pueden transferir volúmenes entre proyectos
- Se puede subir un volumen a una imagen (para arrancar desde él)
- Se puede hacer backup de un volumen (en nuestro caso no)

- Creación de instancias creando un volumen
- Creación de instancias desde volúmenes

**Borrar un Servicio de Alamacenamiento de Cinder: lvmFast02**

[horizon]

- Miramos que no tengamos ninguna instancia corriendo en ese backend de disco (migrar a otro volumen)
- Borramos en Volumen\Tipos de Volumenes la referencia al backend (Disco_Rapido02)

[Storage01]

- Borramos las entradas que hacen referencia en el fichero /etc/cinder/cinder.conf

```ini
enabled_backends = lvmFast01,lvmFast02,lvmSlow01

[lvmFast02]
volume_driver = cinder.volume.drivers.lvm.LVMVolumeDriver
volume_group = cinderv2-Fast
iscsi_protocol = iscsi
iscsi_helper = tgtadm
iscsi_ip_address = 172.16.1.151
volume_clear = none
volume_backend_name = lvmFast02
```

- Reiniciamos el servicio volume

```shell
service cinder-volume restart
```

[Controllers]

- Modificamos la entrada enabled_backends del fichero /etc/cinder/cinder.conf para que no aparezca el backend eleminado
- Reiniciamos el servicio cinder scheduler

```shell
service cinder-scheduler restart
```

- Cargamos los sources de admin para openstack

```shell
cd /home/hvsistemas/
. adminrc
```

- Miramos los servicios de volumen que tenemos:

```shell
openstack volume service list
+------------------+---------------------+------+---------+-------+----------------------------+
| Binary           | Host                | Zone | Status  | State | Updated At                 |
+------------------+---------------------+------+---------+-------+----------------------------+
| cinder-scheduler | controller02        | nova | enabled | up    | 2018-05-15T10:36:07.000000 |
| cinder-scheduler | controller01        | nova | enabled | up    | 2018-05-15T10:36:07.000000 |
| cinder-volume    | storage01@lvmFast01 | nova | enabled | up    | 2018-05-15T10:36:08.000000 |
| cinder-volume    | storage01@lvmFast02 | nova | enabled | up    | 2018-05-15T10:36:08.000000 |
| cinder-volume    | storage01@lvmSlow01 | nova | enabled | up    | 2018-05-15T10:36:09.000000 |
+------------------+---------------------+------+---------+-------+----------------------------+

```

- Deshabilitamos el servicio storage01@lvmFast02

```shell
openstack volume service set storage01@lvmFast02 cinder-volume --disable
```

- Borramos el servicio

```shell
cinder-manage service remove cinder-volume storage01@lvmFast02
```
