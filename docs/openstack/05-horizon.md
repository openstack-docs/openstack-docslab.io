# 5. Horizon

## 5.1 Instalación

**controller01**

Instalamos los paquetes para Horizon:

```shell
sudo apt install openstack-dashboard
```

## 5.2 Configuración

**controllers**

Configuramos Horizon editando el fichero `/etc/openstack-dashboard/local_settings.py`:

```python
OPENSTACK_HOST = "172.16.100.1x"
OPENSTACK_KEYSTONE_URL = "http://%s:5000/v3" % OPENSTACK_HOST
OPENSTACK_KEYSTONE_DEFAULT_ROLE = "user"

TIME_ZONE = "Europe/Madrid"

CACHES = {
    'default': {
         'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
         'LOCATION': '172.16.100.1x:11211',
    }
}
SESSION_ENGINE = 'django.contrib.sessions.backends.cache'

OPENSTACK_API_VERSIONS = {
    "identity": 3,
    "image": 2,
    "volume": 2,
}

DEFAULT_THEME = 'default'
```

Reiniciamos los servicios:

```shell
sudo service apache2 restart
```

## 5.3 Laboratorio

### 5.3.1 Consolas VNC

**kvm01 kvm02**

Modificamos el acceso a la consola vnc en `/etc/nova/nova.conf`:

```ini
novncproxy_base_url = http://public-api.mydomain.local:6080/vnc_auto.html
```

Reiniciamos el servicio:

```shell
sudo service nova-compute restart
```

## 5.4 Comprobaciones

**internet**

Accedemos a http://openstack.mydomain.local/horizon y nos logueamos con `admin:admin`

