# 6. Operaciones

## 6.0 Revisión

### 6.0.1 Admin

- Identity
  - Projects
  - Users
  - Groups
  - Roles

- Admin
  - Compute
    - Hypervisors
    - Host Aggregates
    - Flavors
  - System
    - Defaults
    - System Information

### 6.0.2 User

- Identity
  - Projects
  - Users

- Project
  - API access
  - Compute
    - Overview
    - Instances
    - Images
    - Key Pairs
  - Network
    - Network Topology
    - Networks
    - Routers
    - Security Groups
    - Floating IPs

## 6.1 Administración

### 6.1.1 Flavors

**horizon (admin)**

Creamos los siguientes `Flavors`:

| Nombre | vCPUs | RAM  | Root disk | Ephemeral disk | Swap |
|--------|-------|------|-----------|----------------|------|
| tiny   | 1     | 1024 | 5         | 0              | 0    |
| small  | 2     | 2048 | 5         | 0              | 0    |

### 6.1.2 Networks

**controller01**

Creamos las redes compartidas:

```shell
source adminrc

openstack network create --share --provider-network-type flat --provider-physical-network provider --external provider

openstack subnet create --network provider --subnet-range 192.168.111.0/24 --allocation-pool start=192.168.111.80,end=192.168.111.110 --dns-nameserver 8.8.8.8 --gateway 192.168.111.2 provider
```

**NOTA:** El nombre `provider` debe coincidir con el nombre que hayamos configurado en Neutron

## 6.2 Usuario

**horizon (hvsistemas)**

- Red `internal`
- Router `router01`
- Security group `default`
- Floating IP

- Keypair `hvsistemas` - la copiamos en `local`

- Instance `ubuntu01`
  - Overview
  - Log
  - Console
  - Action Log

- Floating IP
- Ping
- SSH

- Interfaz:
  - IP local
  - MTU (`/etc/neutron/plugins/ml2/ml2_conf.ini`)
  - Salida al exterior
- Flavor:
  - CPU
  - Memoria
  - Disco

- Snapshots
  - Antes de hacer el snapshot instalamos apache2 y creamos una web sencilla
  - Se bloquea la instancia durante un periodo corto
  - Se crea el snapshot como una imagen (la gestion Glance)
  - Podemos crear instancias a partir de los snapshots

- Operaciones
  - Pause / Resume
  - Suspend / Resume
  - Shelve (detiene la instancia y hace un snapshot) / Unshelve (levanta la instancia y borra el snapshot)
  - Resize: necesita que el usuario nova pueda acceder a todos los hosts con par de claves
  - Lock: no permite hacer operaciones peligrosas
  - Soft reboot
  - Hard reboot
  - Shutoff / Start
  - Rebuild
  - Delete

El resize requiere que los nodos de computación se comuniquen a través de ssh sin contraseña.

**kvm01**
Creamos un par de claves ssh y las ponemos en el directorio `~nova/.ssh/`:

```shell
cd ~nova
sudo ssh-keygen -b 4096 -t rsa -C "Key for Nova" -f id_rsa-nova
sudo chmod 400 id_rsa-nova*

sudo mkdir .ssh
sudo chmod 700 .ssh
sudo mv id_rsa-nova* .ssh/
```

Creamos el fichero `~nova/.ssh/config`:
```py
Host 172.16.100.10* kvm*
  IdentityFile ~/.ssh/id_rsa-nova
  StrictHostKeyChecking no
  UserKnownHostsFile=/dev/null
```

Creamos el fichero `~nova/.ssh/authorized_keys` y añadimos la clave pública `id_rsa-nova.pub`

Le damos los propietarios de todo al usuario `nova`:

```shell
sudo chown -R nova:nova ~nova/.ssh/
```

Habilitamos la shell para el usuario `nova`:

```shell
sudo usermod -s /bin/bash nova
```

**kvm02**
Creamos el directorio `~nova/.ssh/`:

```shell
cd ~nova
sudo mkdir .ssh
```

Copiamos las claves desde `kvm01` a `~nova/.ssh/`

Creamos el fichero `~nova/.ssh/config`:

```py
Host 172.16.100.10* kvm*
  IdentityFile ~/.ssh/id_rsa-nova
  StrictHostKeyChecking no
  UserKnownHostsFile=/dev/null
```

Creamos el fichero `~nova/.ssh/authorized_keys` y añadimos la clave pública `id_rsa-nova.pub`

Le damos los propietarios de todo al usuario `nova`:

```shell
sudo chown -R nova:nova ~nova/.ssh/
```

Habilitamos la shell para el usuario `nova`:

```shell
sudo usermod -s /bin/bash nova
```

## 6.3 Cloud Init

[Documentación](http://cloudinit.readthedocs.io/en/latest/)

Siempre se empieza con `#cloud-config`

- Cambiar el username del usuario por defecto:

```yaml
system_info:
  default_user:
    name: hvsistemas
```

- Añadir contraseña a los usuarios:

```yaml
chpasswd:
  list: |
    hvsistemas:hvsistemas
  expire: False
```

- Actualizar paquetes:

```yaml
package_update: true
package_upgrade: true
```

- Instalar paquetes:

```yaml
packages:
  - htop
  - apache2
```

- Escritura de ficheros:

```yaml
write_files:
  - path: /var/www/html/index.html
    permissions: '0644'
    owner: www-data:www-data
    content: |
      Mi web
```

- Ejecución de scripts (sólo una vez):

```yaml
runcmd:
  - git clone https://github.com/taigaio/taiga-blog /home/hvsistemas/taiga-blog/
```
