# 2. Glance

## 2.1 Instalación

**controllers**

**NOTA:** a día 08/02/2018 no hemos podido implementar la v3 de la api de Cinder en Horizon, no la soporta.

Instalamos los paquetes para Glance:

```shell
sudo apt install glance
```

## 2.2 Base de datos

**controller01**

Creamos la base de datos:

`sudo mysql -uroot -p`

```sql
CREATE DATABASE glance;
GRANT ALL PRIVILEGES ON glance.* TO 'glanceuser'@'localhost' IDENTIFIED BY 'glancepass';
GRANT ALL PRIVILEGES ON glance.* TO 'glanceuser'@'%' IDENTIFIED BY 'glancepass';
```

**Controllers**
Editamos el fichero `/etc/glance/glance-api.conf`:

```ini
[database]
#connection = <None>
connection = mysql+pymysql://glanceuser:glancepass@controller/glance
```

**Controller01**
Poblamos la base de datos:

```shell
sudo su -s /bin/sh -c "glance-manage db_sync" glance
```

## 2.3 Servicio, usuario y endpoints

**controller01**

Creamos el usuario `glance` y lo añadimos al proyecto `service` con el rol `admin`:

```shell
source adminrc

openstack user create --domain default --password gl4nc3 glance

openstack role add --project service --user glance admin
```

Creamos el servicio `glance`:

```shell
openstack service create --name glance --description "OpenStack Image" image
```

Creamos los endpoints para Glance:

```shell
openstack endpoint create --region SanSebastian image public http://public-api.mydomain.local:9292
openstack endpoint create --region SanSebastian image internal http://controller:9292
openstack endpoint create --region SanSebastian image admin http://controller:9292
```

## 2.4 Configuración

**controllers**

Configuramos `glance-api` editando el fichero `/etc/glance/glance-api.conf`:

```ini
[DEFAULT]
bind_host = controller0x  
bind_port = 9292
registry_host = controller
registry_port = 9191

[database]
#connection = <None>
connection = mysql+pymysql://glanceuser:glancepass@controller/glance

enable_v1_api=False
enable_v2_api=True

[keystone_authtoken]
auth_uri = http://public-api.mydomain.local:5000
auth_url = http://controller:35357
auth_type = password
project_domain_name = default
user_domain_name = default
project_name = service
username = glance
password = gl4nc3

[paste_deploy]
# ...
flavor = keystone

[glance_store]
# ...
stores = file,http
default_store = file
filesystem_store_datadir = /var/lib/glance/images/
```

Configuramos `glance-registry` editando el fichero `/etc/glance/glance-registry.conf`:

```ini
[DEFAULT]
bind_host = controller0x
bind_port = 9191

[database]
#connection = <None>
connection = mysql+pymysql://glanceuser:glancepass@controller/glance

[keystone_authtoken]
auth_uri = http://public-api.mydomain.local:5000
auth_url = http://controller:35357
auth_type = password
project_domain_name = default
user_domain_name = default
project_name = service
username = glance
password = gl4nc3

service_token_roles_required = true

memcached_servers = controller01:11211, controller02:11211
memcache_dead_retry = 300
memcache_socket_timeout = 3

[paste_deploy]
# ...
flavor = keystone
```

Reiniciamos los servicios:

```shell
sudo service glance-registry restart
sudo service glance-api restart
```

>>> **NOTA:** Si queremos que las imágenes de Glance estén en ambos controllers, o montamos `/var/lib/glance/images/` por nfs o usamos el método de rsync de fernet

`/etc/cron.d/rsync_glance`:

```shell
0 * * * * root  /usr/bin/rsync -av /var/lib/glance/images/ controller02:/var/lib/glance/images/ >/dev/null 2>&1
```

## 2.5 Comprobación

### 2.5.1 Administrador

**controller01**

Descargamos una imagen Cirros:

```shell
wget http://download.cirros-cloud.net/0.3.5/cirros-0.3.5-x86_64-disk.img
```

Subimos la imagen a Glance haciéndola pública:

```shell
source adminrc

openstack image create "Cirros 0.3.5"  --file cirros-0.3.5-x86_64-disk.img --disk-format qcow2 --container-format bare --public
```

Comprobamos que se ha subido correctamente:

```shell
openstack image list
```

### 2.5.2 Usuario

**local**

Descargamos una imagen de Ubuntu:

```shell
wget https://cloud-images.ubuntu.com/xenial/20180627/xenial-server-cloudimg-amd64-disk1.img
```

Subimos la imagen a Glance haciéndola privada:

```shell
source hvsistemasrc

openstack image create --file xenial-server-cloudimg-amd64-disk1.img --disk-format qcow2 --container-format bare --private "Ubuntu 16.04 (amd64)"
```

Comprobamos que se ha subido correctamente:

```shell
openstack image list
```

Para añadir los metadefs hay que ejecutar el siguiente comando:

```shell
su -s /bin/sh -c "glance-manage db_load_metadefs" glance
```

### HA

Es importante que los repositorios de imágenes estén sincronizados, ya sea por NFS, CEPH, rsync o lo que sea, ya que si no no podremos crear instancias.