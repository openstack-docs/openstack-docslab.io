# 8. Heat

## 8.1 Instalación

**controllers**
Instalamos los paquetes para Heat:

```shell
sudo apt install heat-api heat-api-cfn heat-engine
```

## 8.2 Base de datos

**controller01**
Creamos la base de datos para Heat:

`sudo mysql -p`

```sql
CREATE DATABASE heat;
GRANT ALL PRIVILEGES ON heat.* TO 'heatuser'@'localhost' IDENTIFIED BY 'heatpass';
GRANT ALL PRIVILEGES ON heat.* TO 'heatuser'@'%' IDENTIFIED BY 'heatpass';
```

**Controllers**
Editamos el fichero `/etc/heat/heat.conf`:

```ini
[database]
connection = mysql+pymysql://heatuser:heatpass@controller/heat
```

**Controller01**
Poblamos la base de datos:

```shell
sudo su -s /bin/sh -c "heat-manage db_sync" heat
```

## 8.3 Servicios, usuarios y endpoints

**controller01**
Creamos el usuario `heat` y lo añadimos al proyecto `service` con el rol `admin`:

```shell
source adminrc

openstack user create --domain default --password h34t heat

openstack role add --project service --user heat admin
```

Creamos el servicio `heat`:

```shell
openstack service create --name heat --description "Orchestration" orchestration
```

Cremos los endpoints para Heat:

```shell
openstack endpoint create --region SanSebastian orchestration public http://public-api.mydomain.local:8004/v1/%\(tenant_id\)s
openstack endpoint create --region SanSebastian orchestration internal http://controller:8004/v1/%\(tenant_id\)s
openstack endpoint create --region SanSebastian orchestration admin http://controller:8004/v1/%\(tenant_id\)s
```

Creamos el servicio `heat-cfn`:

```shell
openstack service create --name heat-cfn --description "Orchestration"  cloudformation
```

Creamos los endpoints para Heat CloudFormation:

```shell
openstack endpoint create --region SanSebastian cloudformation public http://public-api.mydomain.local:8000/v1
openstack endpoint create --region SanSebastian cloudformation internal http://controller:8000/v1
openstack endpoint create --region SanSebastian cloudformation admin http://controller:8000/v1
```

Creamos el dominio `heat`:

```shell
openstack domain create --description "Stack projects and users" heat
```

Creamos el usuario `heat_domain_admin` en el dominio `heat` y le damos el rol `admin`:

```shell
openstack user create --domain heat --password h34t heat_domain_admin
openstack role add --domain heat --user-domain heat --user heat_domain_admin admin
```

Creamos el rol `heat_stack_owner` y se lo damos al usuario `hvsistemas` en el proyecto `myproject` (habrá que repetir esto por cada usuario en cada proyecto que queramos que tenga capacidad de orquestar):

```shell
openstack role create heat_stack_owner
openstack role add --project myproject --user hvsistemas heat_stack_owner
```

Creamos el rol `heat_stack_user`:

```shell
openstack role create heat_stack_user
```

## 8.4 Configuración

**controllers**
Configuramos Heat editando el fichero `/etc/heat/heat.conf`:

```ini
[DEFAULT]
bind_host = 172.16.100.1x
transport_url = rabbit://openstackuser:openstackpass@controller01:5672,openstackuser:openstackpass@controller02:5672/openstack
heat_metadata_server_url = http://controller:8000
heat_waitcondition_server_url = http://controller:8000/v1/waitcondition
stack_domain_admin = heat_domain_admin
stack_domain_admin_password = h34t
stack_user_domain_name = heat

[database]
connection = mysql+pymysql://heatuser:heatpass@controller/heat

[keystone_authtoken]
auth_uri = http://public-api.mydomain.local:5000
auth_url = http://controller:35357
auth_type = password
project_domain_name = default
user_domain_name = default
project_name = service
username = heat
password = h34t

backend = dogpile.cache.memcached
memcache_servers = controller01:11211, controller02:11211
memcache_dead_retry = 300
memcache_socket_timeout = 3

[trustee]
auth_type = password
auth_url = http://controller:35357
username = heat
password = h34t
user_domain_name = default

[clients_keystone]
auth_uri = http://controller:35357

[ec2authtoken]
auth_uri = http://public-api.mydomain.local:5000/v3
```

Reiniciamos los servicios:

```shell
sudo service heat-api restart
sudo service heat-api-cfn restart
sudo service heat-engine restart
```

## 8.5 Comprobación

### 8.5.1 Administrador

**controller01**
Comprobamos que están todos los servicios:

```shell
source adminrc

openstack orchestration service list
```

**NOTA:** CREEMOS que el número de servicios de orquestación creados en cada nodo es proporcional al número de núcleos. **(COMPROBAR CON UNA MÁQUINA NUEVA)**

### 8.5.2 Usuario

**local**
Creamos el fichero `template-demo.yml`:

```yaml
heat_template_version: 2015-10-15
description: Launch a basic instance with Ubuntu image using the tiny flavor, hvsistemas key and one network

parameters:
  NetID:
    type: string
    description: Network ID to use for the instance.

resources:
  server:
    type: OS::Nova::Server
    properties:
      image: "Ubuntu 16.04 (amd64)"
      flavor: "tiny"
      key_name: "hvsistemas"
      networks:
      - network: { get_param: NetID }

outputs:
  instance_name:
    description: Name of the instance.
    value: { get_attr: [ server, name ] }
  instance_ip:
    description: IP address of the instance.
    value: { get_attr: [ server, first_address ] }
```

Lanzamos la plantilla `template-demo.yml`:

```shell
source hvsistemasrc

export NET_ID=internal
openstack stack create -t template-demo.yml --parameter "NetID=$NET_ID" mystack
```

Listamos los stacks hasta que esté completo:

```shell
openstack stack list
```

Vemos la salida de la ejecución del stack:

```shell
openstack stack output show --all mystack
```

Listamos las instancias:

```shell
openstack server list
```

Borramos el stack:

```shell
openstack stack delete --yes mystack
```

Lanzamos el stack `mylamp` desde el fichero `template-lamp.yml`:

```shell
openstack stack create -t template-lamp.yml mylamp
```

Listamos los stacks hasta que esté completo:

```shell
openstack stack list
```

Vemos la salida de la ejecución del stack:

```shell
openstack stack output show --all mylamp
openstack stack output show mylamp deploy_ip  -c output_value -f value
```
