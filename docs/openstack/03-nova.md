# 3. Nova

## 3.0 Teoría

- [Cells](https://docs.openstack.org/nova/latest/user/cells.html)
- [Placement API](https://docs.openstack.org/nova/latest/user/placement.html)

## 3.1 Instalación

### 3.1.1 Controladores

**controller01**

Instalamos los paquetes para Nova:

```shell
sudo apt install nova-api nova-conductor nova-consoleauth nova-novncproxy nova-scheduler nova-placement-api
```

### 3.1.2 Computación

**kvm01 kvm02**

Instalamos los paquetes para Nova:

```shell
sudo apt install nova-compute
```

## 3.2 Base de datos

**controller01**

Creamos las bases de datos:

`sudo mysql -p`

```sql
CREATE DATABASE nova;
GRANT ALL PRIVILEGES ON nova.* TO 'novauser'@'localhost' IDENTIFIED BY 'novapass';
GRANT ALL PRIVILEGES ON nova.* TO 'novauser'@'%' IDENTIFIED BY 'novapass';

CREATE DATABASE nova_api;
GRANT ALL PRIVILEGES ON nova_api.* TO 'novauser'@'localhost' IDENTIFIED BY 'novapass';
GRANT ALL PRIVILEGES ON nova_api.* TO 'novauser'@'%' IDENTIFIED BY 'novapass';

CREATE DATABASE nova_cell0;
GRANT ALL PRIVILEGES ON nova_cell0.* TO 'novauser'@'localhost' IDENTIFIED BY 'novapass';
GRANT ALL PRIVILEGES ON nova_cell0.* TO 'novauser'@'%' IDENTIFIED BY 'novapass';
```

## 3.3 Servicios, usuarios y endpoints

**controller01**

Creamos el usuario `nova` y lo añadimos al proyecto `service` con el rol `admin`:

```shell
source adminrc

openstack user create --domain default --password n0v4 nova

openstack role add --project service --user nova admin
```

Creamos el servicio `nova`:

```shell
openstack service create --name nova --description "OpenStack Compute" compute
```

Creamos los endpoints para Nova:

```shell
openstack endpoint create --region SanSebastian compute public http://public-api.mydomain.local:8774/v2.1
openstack endpoint create --region SanSebastian compute internal http://controller:8774/v2.1
openstack endpoint create --region SanSebastian compute admin http://controller:8774/v2.1
```

Creamos el usuario `placement` y lo añadimos al proyecto `service` con el rol `admin`:

```shell
source adminrc

openstack user create --domain default --password pl4c3m3nt placement

openstack role add --project service --user placement admin
```

Creamos el servicio `placement`:

```shell
openstack service create --name placement --description "Placement API" placement
```

Creamos los endpoints para Placement:

```shell
openstack endpoint create --region SanSebastian placement public http://public-api.mydomain.local:8778
openstack endpoint create --region SanSebastian placement internal http://controller:8778
openstack endpoint create --region SanSebastian placement admin http://controller:8778
```

## 3.4 Configuración

### 3.4.1 Controladores

**controllers**

Configuramos Nova editando el fichero `/etc/nova/nova.conf`:

```ini
[DEFAULT]
transport_url = rabbit://openstackuser:openstackpass@controller01:5672,openstackuser:openstackpass@controller02:5672/openstack
bind_host = controller0x
my_ip = controller0X
use_neutron = true
firewall_driver = nova.virt.firewall.NoopFirewallDriver
osapi_compute_listen = $my_ip

[database]
connection = mysql+pymysql://novauser:novapass@controller/nova

[api_database]
connection = mysql+pymysql://novauser:novapass@controller/nova_api

[cache]
backend = dogpile.cache.memcached
enabled = true
memcache_servers = controller01:11211, controller02:11211
memcache_dead_retry = 300
memcache_socket_timeout = 3


[api]
auth_strategy = keystone

[keystone_authtoken]
auth_uri = http://public-api.mydomain.local:5000
auth_url = http://controller:35357
auth_type = password
project_domain_name = default
user_domain_name = default
project_name = service
username = nova
password = n0v4

backend = dogpile.cache.memcached
memcache_servers = controller01:11211, controller02:11211
memcache_dead_retry = 300
memcache_socket_timeout = 3

[vnc]
enabled = true
vncserver_listen = $my_ip
vncserver_proxyclient_address = $my_ip

novncproxy_host = $my_ip

[glance]
api_servers = http://controller:9292

[oslo_concurrency]
lock_path = /var/lib/nova/tmp

[placement]
os_region_name = SanSebastian
project_domain_name = Default
project_name = service
auth_type = password
user_domain_name = Default
auth_url = http://controller:35357/v3
username = placement
password = pl4c3m3nt

[oslo_messaging_rabbit]
rabbit_retry_interval = 1
rabbit_retry_backoff = 2
rabbit_max_retries = 0
rabbit_ha_queues = true

```

### 3.4.2 Computación

**kvm01 kvm02**

Configuramos Nova editando el fichero `/etc/nova/nova.conf`:

```ini
[DEFAULT]
transport_url = rabbit://openstackuser:openstackpass@controller01:5672,openstackuser:openstackpass@controller02:5672/openstack
my_ip = 172.16.100.10x
use_neutron = true
firewall_driver = nova.virt.firewall.NoopFirewallDriver

block_device_allocate_retries = 600
block_device_allocate_retries_interval = 10


[api]
auth_strategy = keystone

[keystone_authtoken]
auth_uri = http://public-api.mydomain.local:5000
auth_url = http://controller:35357
auth_type = password
project_domain_name = default
user_domain_name = default
project_name = service
username = nova
password = n0v4

backend = dogpile.cache.memcached
memcache_servers = controller01:11211, controller02:11211
memcache_dead_retry = 300
memcache_socket_timeout = 3

[vnc]
enabled = true
vncserver_listen = 0.0.0.0
vncserver_proxyclient_address = $my_ip
novncproxy_base_url = http://public-api.mydomain.local:6080/vnc_auto.html

[glance]
api_servers = http://controller:9292

[oslo_concurrency]
lock_path = /var/lib/nova/tmp

[placement]
os_region_name = SanSebastian
project_domain_name = Default
project_name = service
auth_type = password
user_domain_name = Default
auth_url = http://controller:35357/v3
os_interface = admin
username = placement
password = pl4c3m3nt
```

**NOTA:** Nos aseguramos que hemos cambiado la `x` por `1` en caso de `kvm01` y `2` en caso de `kvm02`

Configuramos el tipo de virtualización editando el fichero `/etc/nova/nova-compute.conf`:

```ini
[libvirt]
virt_type = kvm
```

## 3.5 Inicialización

### 3.5.1 Controladores

**controller01**

Inicializamos las bases de datos:

```shell
sudo su -s /bin/sh -c "nova-manage api_db sync" nova
sudo su -s /bin/sh -c "nova-manage cell_v2 map_cell0" nova
sudo su -s /bin/sh -c "nova-manage cell_v2 create_cell --name=cell1 --verbose" nova
sudo su -s /bin/sh -c "nova-manage db sync" nova
```

Comprobamos que se han registrado correctamente `cell0` y `cell1`:

```shell
sudo nova-manage cell_v2 list_cells
```

Reiniciamos los servicios:

```shell
sudo service nova-api restart
sudo service nova-consoleauth restart
sudo service nova-scheduler restart
sudo service nova-conductor restart
sudo service nova-novncproxy restart
```

o

```shell
service nova-api restart && service nova-consoleauth restart && service nova-scheduler restart && service nova-conductor restart && service nova-novncproxy restart
```

### 3.5.2 Computación

**kvm01 kvm02**

Reiniciamos los servicios:

```shell
sudo service nova-compute restart
```

Por si acaso reiniciamos los KVM

**controller01**

Damos de alta los nodos de computación:

```shell
sudo nova-manage cell_v2 discover_hosts --verbose
```

>Found 2 cell mappings.
>Skipping cell0 since it does not contain hosts.
>Getting compute nodes from cell 'cell1': 5f4088aa-48c1-4aba-bdfa-b92b4a43e161
>Found 2 unmapped computes in cell: 5f4088aa-48c1-4aba-bdfa-b92b4a43e161
>Checking host mapping for compute host 'kvm01': 36352569-a283-462d-8c45-8be6a18e9ea9
>Creating host mapping for compute host 'kvm01': 36352569-a283-462d-8c45-8be6a18e9ea9
>Checking host mapping for compute host 'kvm02': 2593bb79-bfbf-4d6e-bdf4-5ed5559e913d
>Creating host mapping for compute host 'kvm02': 2593bb79-bfbf-4d6e-bdf4-5ed5559e913d

El resultado de `nova-status upgrade check`:

```sql
+---------------------------+
| Upgrade Check Results     |
+---------------------------+
| Check: Cells v2           |
| Result: Success           |
| Details: None             |
+---------------------------+
| Check: Placement API      |
| Result: Success           |
| Details: None             |
+---------------------------+
| Check: Resource Providers |
| Result: Success           |
| Details: None             |
+---------------------------+
```

## 3.6 Comprobación

**controller01**

```shell
source adminrc

openstack compute service list --service nova-compute
openstack compute service list
openstack catalog list
openstack image list
sudo nova-status upgrade check
```

## 3.7. Extra

**kvm01 y kvm02**
Si queremos reanudar el estado de las instancias al reiniciar el host, hay que añadir lo siguiente a `/etc/nova/nova.conf`:

```ini
resume_guests_state_on_host_boot = true
```

En el fichero `/etc/nova/nova.conf` en hay que apuntar a la IP y no al nombre de host en la siguiente línea:

```ini
novncproxy_base_url = http://public-api.mydomain.local:6080/vnc_auto.html
```

## 3.8. Borrar celdas:

```shell
nova-manage cell_v2 list_cells
nova-manage cell_v2 delete_cell --force --cell_uuid XXXXXXXXXXXXXXXXXXXX
```

## 3.9. Buscar el host de computacion en un celda mas insistetemente:

```shell
nova-manage cell_v2 discover_hosts --verbose --by-service
```
