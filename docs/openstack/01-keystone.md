# 1. Keystone

## 1.0 Teoría

- [Fernet](https://docs.openstack.org/keystone/pike/admin/identity-fernet-token-faq.html)


## 1.1 Instalación

**controllers**

Instalamos los paquetes para Keystone:

```shell
sudo apt install keystone memcached
```

## 1.2 Base de datos

**controllers**

Creamos la base de datos:

`sudo mysql`

```sql
CREATE DATABASE keystone;
GRANT ALL PRIVILEGES ON keystone.* TO 'keystoneuser'@'localhost' IDENTIFIED BY 'keystonepass';
GRANT ALL PRIVILEGES ON keystone.* TO 'keystoneuser'@'%' IDENTIFIED BY 'keystonepass';
```

Editamos el fichero `/etc/keystone/keystone.conf`:
```ini
[default]
bind_host = 172.16.100.1x
public_bind_host = 192.168.111.1x
admin_bind_host = 172.16.100.1x

[database]
#connection = sqlite:////var/lib/keystone/keystone.db
connection = mysql+pymysql://keystoneuser:keystonepass@controller/keystone

[cache]
enabled = true
backend = dogpile.cache.memcached
memcache_servers = controller01:11211, controller02:11211
memcache_dead_retry = 300
memcache_socket_timeout = 3
```

Reiniciamos Apache :

```shell
service apache2 restart
```

**Controller01**
Poblamos la base de datos:

```shell
sudo su -s /bin/sh -c "keystone-manage db_sync" keystone
```

## 1.3 Inicialización

**controller01**

Inicializamos los repositorios de claves Fernet:

```shell
sudo keystone-manage fernet_setup --keystone-user keystone --keystone-group keystone
sudo keystone-manage credential_setup --keystone-user keystone --keystone-group keystone
```

Para que los nodos de Keystone funcionen correctamente, el repositorio de claves Fernet tiene que estar replicado.

Ahora rotamos las claves y las sincronizamos en el resto de nodos

```shell
keystone-manage fernet_rotate --keystone-user keystone --keystone-group keystone
rsync -a -v /etc/keystone/fernet-keys/ controller02:/etc/keystone/fernet-keys/
```

Y añadimos la sincronización al cron `/etc/cron.d/rsync_fernet`:

```ini
0 * * * * root  /usr/bin/rsync -a -v /etc/keystone/fernet-keys/ controller02:/etc/keystone/fernet-keys/ >/dev/null 2>&1
```

**NOTA:** public-api.mydomain.local tiene que resolver a la ip virtual pública

Inicializamos los endpoints de Keystone:

```shell
sudo keystone-manage bootstrap --bootstrap-password admin \
  --bootstrap-public-url http://public-api.mydomain.local:5000/v3/ \
  --bootstrap-internal-url http://controller:5000/v3/ \
  --bootstrap-admin-url http://controller:35357/v3/ \
  --bootstrap-region-id SanSebastian
```

Exportamos las siguientes variables de entorno:

```shell
export OS_USERNAME=admin
export OS_PASSWORD=admin
export OS_PROJECT_NAME=admin
export OS_USER_DOMAIN_NAME=Default
export OS_PROJECT_DOMAIN_NAME=Default
export OS_AUTH_URL=http://controller:35357/v3
export OS_IDENTITY_API_VERSION=3
```

## 1.4 Proyectos, roles y usuarios

**controller01**

Creamos los proyectos `service` y `myproject`:

```shell
openstack project create --domain default --description "Service Project" service
openstack project create --domain default --description "myproject Project" myproject
```

Creamos el usuario `hvsistemas`:

```shell
openstack user create --domain default --password hvsistemas hvsistemas
```

Creamos el rol `user`:

```shell
openstack role create user
```

Añadimos al usuario `hvsistemas` en el proyecto `myproject` con rol `user`:

```shell
openstack role add --project myproject --user hvsistemas user
```

Eliminamos las variables de entorno creadas anteriormente:

```shell
unset OS_USERNAME
unset OS_PASSWORD
unset OS_PROJECT_NAME
unset OS_USER_DOMAIN_NAME
unset OS_PROJECT_DOMAIN_NAME
unset OS_AUTH_URL
unset OS_IDENTITY_API_VERSION
```

Probamos a pedir un token con el usuario `admin`:

```shell
openstack \
  --os-username admin \
  --os-project-name admin \
  --os-user-domain-name Default \
  --os-project-domain-name Default \
  --os-auth-url http://controller:35357/v3 \
  --os-identity-api-version 3 \
  token issue
```

Probamos a pedir un token con el usuario `hvsistemas`:

```shell
openstack \
  --os-username hvsistemas \
  --os-project-name myproject \
  --os-user-domain-name Default \
  --os-project-domain-name Default \
  --os-auth-url http://public-api.mydomain.local:5000/v3 \
  --os-identity-api-version 3 \
  token issue
```


## 1.5 Apache

**controller01**

Keystone se sirve con `mod_wsgi` de Apache.
Se puede ver la configuración en `/etc/apache2/sites-enabled/keystone.conf`
Si queremos reiniciar Keystone, debemos reiniciar el servicio `apache2`

## 1.6 Ficheros de acceso

**controller01**

Por facilidad, vamos a crear un fichero en `/home/hvsistemas/adminrc` con el contenido:

```shell
export OS_USERNAME=admin
export OS_PASSWORD=admin
export OS_PROJECT_NAME=admin
export OS_USER_DOMAIN_NAME=Default
export OS_PROJECT_DOMAIN_NAME=Default
export OS_AUTH_URL=http://controller:35357/v3
export OS_IDENTITY_API_VERSION=3
export OS_IMAGE_API_VERSION=2
```

Por facilidad, vamos a crear un fichero en `/home/hvsistemas/hvsistemasrc` con el contenido:

```shell
export OS_USERNAME=hvsistemas
export OS_PASSWORD=hvsistemas
export OS_PROJECT_NAME=myproject
export OS_USER_DOMAIN_NAME=Default
export OS_PROJECT_DOMAIN_NAME=Default
export OS_AUTH_URL=http://public-api.mydomain.local:5000/v3
export OS_IDENTITY_API_VERSION=3
export OS_IMAGE_API_VERSION=2
```

Ahora podemos cargar las variables de entorno con estos ficheros:

```shell
source adminrc
openstack token issue

source hvsistemasrc
openstack token issue
```
