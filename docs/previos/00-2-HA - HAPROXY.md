# HAPROXY

## 1. Instalación

**Controllers**

```shell
apt install haproxy hatop
```

## 2. Configuración

### 2.1. Rsyslog

Para que HAProxy pueda logear eventos en un fichero local, tenemos que hacer un cambio en `/etc/rsyslog.d/49-haproxy.conf`. Debería quedarnos así:

```python
# Create an additional socket in haproxy's chroot in order to allow logging via
# /dev/log to chroot'ed HAProxy processes
#$AddUnixListenSocket /var/lib/haproxy/dev/log
#
# Send HAProxy messages to a dedicated logfile
#if $programname startswith 'haproxy' then /var/log/haproxy.log
#&~

# .. otherwise consider putting these two in /etc/rsyslog.conf instead:
$ModLoad imudp
$UDPServerAddress 127.0.0.1
$UDPServerRun 514

# ..and in any case, put these two in /etc/rsyslog.d/49-haproxy.conf:
local0.* -/var/log/haproxy_0.log
& ~
# & ~ means not to put what matched in the above line anywhere else for the rest of the rules
# http://serverfault.com/questions/214312/how-to-keep-haproxy-log-messages-out-of-var-log-syslog

```

Y reiniciamos el servicio:

```shell
service rsyslog restart
```

### 2.2. HAProxy

> **IMPORTANTE:**  **user haproxy** en el chequeo de Galera, creado en la BBDD con un `create user 'haproxy'@'172.16.100.%';`

Hay que balancear todos los servicios, así que el fichero `/etc/haproxy/haproxy.cfg` quedaría así, más o menos:

```python
global
  log 127.0.0.1 local0 debug
#  log /var/lib/haproxy/dev/log local0 debug
  chroot  /var/lib/haproxy
  daemon
  group  haproxy
  maxconn  4000
  pidfile  /var/run/haproxy.pid
  user  haproxy
  stats socket /var/run/haproxy.sock mode 600 level admin
  stats timeout 2m

defaults
  log  global
  maxconn  4000
  option  redispatch
  retries  3
  timeout  http-request 10s
  timeout  queue 1m
  timeout  connect 10s
  timeout  client 1m
  timeout  server 1m
  timeout  check 10s

 listen rabbit_cluster
#  bind 172.16.100.10:5672
#  balance  source
  option  tcpka
#  option  httpchk
  option  tcplog
  server controller01 172.16.100.11:5672 check inter 2000 rise 2 fall 5
  server controller02 172.16.100.12:5672 check inter 2000 rise 2 fall 5
#  server controller03p 192.168.111.13:5672 check inter 2000 rise 2 fall 5

 listen dashboard_cluster
  bind 192.168.111.10:80
  balance  source
  option  tcpka
  option  httpchk
  option  tcplog
  server controller01p 192.168.111.11:80 check inter 2000 rise 2 fall 5
  server controller02p 192.168.111.12:80 check inter 2000 rise 2 fall 5
#  server controller03p 192.168.111.13:80 check inter 2000 rise 2 fall 5

 listen galera_cluster
  bind 192.168.111.10:3306
  bind 172.16.100.10:3306
  balance  source
  option  mysql-check user haproxy
  server controller01 172.16.100.11:3306 check inter 2000 rise 2 fall 5
  server controller02 172.16.100.12:3306 backup check inter 2000 rise 2 fall 5
#  server controller03 172.16.100.13:3306 backup check inter 2000 rise 2 fall 5

 listen glance_api_cluster
  bind 172.16.100.10:9292
  bind 192.168.111.10:9292
  balance  source
  option  tcpka
  option  httpchk
  option  tcplog
  server controller01 172.16.100.11:9292 check inter 2000 rise 2 fall 5
  server controller02 172.16.100.12:9292 check inter 2000 rise 2 fall 5
#  server controller03 172.16.100.13:9292 check inter 2000 rise 2 fall 5

 listen glance_registry_cluster
  bind 172.16.100.10:9191
  bind 192.168.111.10:9191
  balance  source
  option  tcpka
  option  tcplog
  server controller01 172.16.100.11:9191 check inter 2000 rise 2 fall 5
  server controller02 172.16.100.12:9191 check inter 2000 rise 2 fall 5
#  server controller03 172.16.100.13:9191 check inter 2000 rise 2 fall 5
#
 listen keystone_admin_cluster
  bind 172.16.100.10:35357
  balance  source
  option  tcpka
  option  httpchk
  option  tcplog
  server controller01 172.16.100.11:35357 check inter 2000 rise 2 fall 5
  server controller02 172.16.100.12:35357 check inter 2000 rise 2 fall 5
#  server controller03 172.16.100.13:35357 check inter 2000 rise 2 fall 5


listen keystone_public_internal_cluster
  bind 192.168.111.10:5000
  balance  source
  option  tcpka
  option  httpchk
  option  tcplog
  server controller01p 192.168.111.11:5000 check inter 2000 rise 2 fall 5
  server controller02p 192.168.111.12:5000 check inter 2000 rise 2 fall 5
#  server controller03p 192.168.111.13:5000 check inter 2000 rise 2 fall 5

 listen keystone_internal_cluster
  bind 172.16.100.10:5000
  balance  source
  option  tcpka
  option  httpchk
  option  tcplog
  server controller01 172.16.100.11:5000 check inter 2000 rise 2 fall 5
  server controller02 172.16.100.12:5000 check inter 2000 rise 2 fall 5
#  server controller03p 192.168.111.13:5000 check inter 2000 rise 2 fall 5


# listen nova_ec2_api_cluster
#  bind 172.16.100.10:8773
#  balance  source
#  option  tcpka
#  option  tcplog
#  server controller01 172.16.100.11:8773 check inter 2000 rise 2 fall 5
#  server controller02 172.16.100.12:8773 check inter 2000 rise 2 fall 5
##  server controller03 172.16.100.13:8773 check inter 2000 rise 2 fall 5

 listen nova_compute_api_cluster
  bind 172.16.100.10:8774
  bind 192.168.111.10:8774
  balance  source
  option  tcpka
  option  httpchk
  option  tcplog
  server controller01 172.16.100.11:8774 check inter 2000 rise 2 fall 5
  server controller02 172.16.100.12:8774 check inter 2000 rise 2 fall 5
##  server controller03 172.16.100.13:8774 check inter 2000 rise 2 fall 5
#
 listen nova_metadata_api_cluster
  bind 172.16.100.10:8775
  bind 192.168.111.10:8775
  balance  source
  option  tcpka
  option  tcplog
  server controller01 172.16.100.11:8775 check inter 2000 rise 2 fall 5
  server controller02 172.16.100.12:8775 check inter 2000 rise 2 fall 5
#  server controller03 172.16.100.13:8775 check inter 2000 rise 2 fall 5

 listen nova_placement_api_cluster
  bind 172.16.100.10:8778
  bind 192.168.111.10:8778
  balance  source
  option  tcpka
  option  tcplog
  server controller01 172.16.100.11:8778 check inter 2000 rise 2 fall 5
  server controller02 172.16.100.12:8778 check inter 2000 rise 2 fall 5
#  server controller03 172.16.100.13:8778 check inter 2000 rise 2 fall 5


 listen cinder_api_cluster
  bind 172.16.100.10:8776
  bind 192.168.111.10:8776
  balance  source
  option  tcpka
  option  httpchk
  option  tcplog
  server controller01 172.16.100.11:8776 check inter 2000 rise 2 fall 5
  server controller02 172.16.100.12:8776 check inter 2000 rise 2 fall 5
#  server controller03 172.16.100.13:8776 check inter 2000 rise 2 fall 5

 listen heat_api_cfn_cluster
  bind 172.16.100.10:8000
  bind 192.168.111.10:8000
  balance  source
  option  tcpka
  option  httpchk
  option  tcplog
  server controller01 172.16.100.11:8000 check inter 2000 rise 2 fall 5
  server controller02 172.16.100.12:8000 check inter 2000 rise 2 fall 5
#  server controller03 172.16.100.13:8000 check inter 2000 rise 2 fall 5

listen heat_api_cluster
  bind 172.16.100.10:8004
  bind 192.168.111.10:8004
  balance  source
  option  tcpka
  option  httpchk
  option  tcplog
  server controller01 172.16.100.11:8004 check inter 2000 rise 2 fall 5
  server controller02 172.16.100.12:8004 check inter 2000 rise 2 fall 5
#  server controller03 172.16.100.13:8004 check inter 2000 rise 2 fall 5

 listen gnocchi_api_cluster
  bind 172.16.100.10:8041
  bind 192.168.111.10:8041
  balance  source
  option  tcpka
  option  httpchk
  option  tcplog
  server controller01 172.16.100.11:8041 check inter 2000 rise 2 fall 5
  server controller02 172.16.100.12:8041 check inter 2000 rise 2 fall 5
#  server controller03 172.16.100.13:8041 check inter 2000 rise 2 fall 5


# listen ceilometer_api_cluster
#  bind 172.16.100.10:8777
#  balance  source
#  option  tcpka
#  option  tcplog
#  server controller01 172.16.100.11:8777 check inter 2000 rise 2 fall 5
#  server controller02 172.16.100.12:8777 check inter 2000 rise 2 fall 5
##  server controller03 172.16.100.13:8777 check inter 2000 rise 2 fall 5

 listen nova_vncproxy_cluster
  bind 172.16.100.10:6080
  bind 192.168.111.10:6080
  balance  source
  option  tcpka
  option  tcplog
  server controller01 172.16.100.11:6080 check inter 2000 rise 2 fall 5
  server controller02 172.16.100.12:6080 check inter 2000 rise 2 fall 5
##  server controller03 172.16.100.13:6080 check inter 2000 rise 2 fall 5
#
 listen neutron_api_cluster
  bind 172.16.100.10:9696
  bind 192.168.111.10:9696
  balance  source
  option  tcpka
  option  httpchk
  option  tcplog
  server controller01 172.16.100.11:9696 check inter 2000 rise 2 fall 5
  server controller02 172.16.100.12:9696 check inter 2000 rise 2 fall 5
#  server controller03 172.16.100.13:9696 check inter 2000 rise 2 fall 5


```

También hay que añadir lo siguiente al fichero `/etc/sysctl.conf` para que los controllers puedan escuchar en interfaces de red externas (IP's virtuales):
```
net.ipv4.ip_nonlocal_bind = 1
```

Y aplicar los cambios ejecutando `sysctl -p`

**EN UN CONTROLLER**


Después hay que añadir los recursos y restricciones a Pacemaker:
```
pcs resource create lb-haproxy lsb:haproxy --clone
pcs constraint order start vip then vip2
pcs constraint order start vip2 then lb-haproxy-clone
pcs constraint colocation add vip with vip2
pcs constraint colocation add lb-haproxy-clone with vip
```
```
 pcs status
```
```
Cluster name: openstackv1
Last updated: Wed Apr 18 10:37:00 2018          Last change: Wed Apr 18 10:26:51 2018 by root via cibadmin on controller01
Stack: corosync
Current DC: controller02 (version 1.1.14-70404b0) - partition with quorum
2 nodes and 4 resources configured

Online: [ controller01 controller02 ]

Full list of resources:

 vip    (ocf::heartbeat:IPaddr2):       Started controller01
 Clone Set: lb-haproxy-clone [lb-haproxy]
     Started: [ controller01 ]
     Stopped: [ controller02 ]
 vip2   (ocf::heartbeat:IPaddr2):       Started controller01

PCSD Status:
  controller01: Online
  controller02: Online

Daemon Status:
  corosync: active/enabled
  pacemaker: active/enabled
  pcsd: active/enabled
```

## 3. Comprobación adicional para Galera
**Controller01 y Controller02**
```
apt install xinetd
```

Crear el fichero `/etc/default/clusterchek`:
```
MYSQL_USERNAME="clustercheck_user"
MYSQL_PASSWORD="my_clustercheck_password"
MYSQL_HOST="localhost"
MYSQL_PORT="3306"
```
Añadir el usuario a la base de datos:
```
GRANT PROCESS ON *.* TO 'clustercheck_user'@'localhost' IDENTIFIED BY 'my_clustercheck_password';
GRANT PROCESS ON *.* TO 'clustercheck_user'@'controller01' IDENTIFIED BY 'my_clustercheck_password';
GRANT PROCESS ON *.* TO 'clustercheck_user'@'controller02' IDENTIFIED BY 'my_clustercheck_password';
FLUSH PRIVILEGES;
```



Crear el fichero `/etc/xinetd.d/galera-monitor`:
```
service galera-monitor
{
   port = 9200
   disable = no
   socket_type = stream
   protocol = tcp
   wait = no
   user = root
   group = root
   groups = yes
   server = /usr/bin/clustercheck
   type = UNLISTED
   per_source = UNLIMITED
   log_on_success =
   log_on_failure = HOST
   flags = REUSE
}
```

Iniciar servicio:

```shell
service xinetd enable
service xinetd start
```

**NOTA:** probablemente haya que cambiar el bind_address en los ficheros de configuración de los servicios que corren en los controllers por la propia IP del controller.

**NOTA:** para usar  HAtop lanzar el siguiente comando: `hatop -s /var/run/haproxy.sock`
