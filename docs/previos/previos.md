# 0. Previos

## 0.0 ACHTUNG!

1. Es recomendable añadir todos los hosts de OpenStack al fichero `/etc/hosts` 
2. Desde los nodos de computación las referencias a la IP externa del controller no tienen sentido excepto cuando referencian a la consola VNC
3. Nos aseguramos de añadir lo siguiente al fichero `/etc/hosts`:

```python
172.16.100.11   controller01
192.168.111.11  controller01p
172.16.100.12   controller02
192.168.111.12  controller02p
172.16.100.51   gateway01
172.16.100.52   gateway02
172.16.100.101  kvm01
172.16.100.102  kvm02
172.16.100.151  storage01
## IP's virtuales
172.16.100.10   controller
192.168.111.10  controllerp
```

**Controller01 y 02**
Instalamos memcached :

```shell
apt install memcached
```

### A la hora de configurar memcached, en los controllers lo haremos escuchar por la interfaz de management. En `/etc/memcached.conf`:

```ini
-l controller0x
```

Comprobamos que está escuchando en la interfaz deseada:

```shell
netstat -plutn | grep memcache
```

>>> **NOTA:** Y en los ficheros de configuración de los servicios apuntaremos a las direcciones de todos los controllers en el apartado [cache] .

```ini
[cache]
enabled = true
backend = dogpile.cache.memcached
memcache_servers = controller01:11211, controller02:11211
memcache_dead_retry = 300
memcache_socket_timeout = 3
```

## 0.0.5 SSH SIN CONTRASEÑA PARA ROOT

hacemos lo siguiente en ambos controllers como root:

```shell
ssh-keygen
scp /root/.ssh/id_rsa.pub hvsistemas@controller0x:/tmp/
cat /tmp/id_rsa.pub > /root/.ssh/authorized_keys
```

## 0.1 Actualización de paquetes

**controller01, controller02, gateway01, gateway02, storage01, kvm01 y kvm02**

```shell
sudo apt update
sudo apt dist-upgrade
sudo reboot
```

## 0.2 NTP

**controller01, controller02, gateway01, gateway02, storage01, kvm01 y kvm02**

Instalamos el cliente ntp:

```shell
sudo apt install ntp ntpdate
```

## 0.3 Repositorios OpenStack Pike

**controller01, controller02, gateway01, gateway02, storage01, kvm01 y kvm02**

Añadimos el repositorio para `pike` y actualizamos los paquetes:

```shell
sudo apt install software-properties-common

sudo add-apt-repository cloud-archive:pike
sudo apt update && sudo apt dist-upgrade
```

## 0.4 RabbitMQ

**controller01 y controller02**

Instalamos el _broker_ RabbitMQ:

```shell
sudo apt install rabbitmq-server
```

Modificamos el fichero de configuración `/etc/rabbitmq/rabbitmq-env.conf`:

```ini
NODE_IP_ADDRESS=controller0x
```

En un controller  (el que vayamos a usar como referencia para el HA) creamos el _user_ y _vhost_ y establecemos permisos:

```shell
sudo rabbitmqctl add_user openstackuser openstackpass
sudo rabbitmqctl add_vhost openstack
sudo rabbitmqctl set_permissions -p openstack openstackuser ".*" ".*" ".*"
```

Reiniciamos el servicio:

```shell
sudo service rabbitmq-server restart
```

Seguir en [00-3-HA-RABBIT.md](00-3-HA-RABBIT.md) si queremos alta disponibilidad

## 0.5 MariaDB > para HA Mirar [00-4-HA - MariaDB - Galera.md](00-4-HA-MariaDB-Galera.md)

**controller01**
Instalamos MariaDB:

```shell
sudo apt install mariadb-server python-pymysql
```

Editamos el fichero `/etc/mysql/mariadb.conf.d/50-server.cnf`:

```ini
bind-address          = controller0x
character-set-server  = utf8
collation-server      = utf8_general_ci
```

Reiniciamos el servicio:

```shell
sudo service mysql restart
```

## 0.6 API

**controller01 controller02**

Instalamos los clientes de las APIs:

```shell
sudo apt install python-openstackclient
```

Ejecutamos el siguiente comando para tener bash completion:

```shell
openstack complete | sudo tee /etc/bash_completion.d/osc.bash_completion > /dev/null
```

### Extra extra

comandos útiles:

```shell
openstack usage show --project myproject --start 2018-01-05 --end 2018-01-06
openstack usage list --start 2018-01-05 --end 2018-01-06
```

**Alta disponibilidad**

[Guía oficial](https://docs.openstack.org/ha-guide/intro-ha.html)