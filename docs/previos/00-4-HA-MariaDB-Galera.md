# MariaDB en HA - Galera
## 1. Instalación
**En todos los controllers**
Añadimos el repositorio e instalamos mariadb:

```shell
apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
add-apt-repository 'deb [arch=amd64,i386,ppc64el] http://nyc2.mirrors.digitalocean.com/mariadb/repo/10.1/ubuntu xenial main'
apt update
apt install rsync mariadb-server python-pymysql
```

>**NOTA:** tenemos que asegurarnos de poner contraseña al usuario root de la bbdd para no tener que lidiar con el unix_socket etc.
Creamos el fichero `/etc/mysql/conf.d/galera.cnf` (o en `/etc/mysql/mariadb.conf.d/50-server.cnf`):

```ini
[mysqld]
datadir=/var/lib/mysql
#socket=/var/lib/mysql/mysql.sock
user=mysql
binlog_format=ROW
bind-address=172.16.100.1x
skip-external-locking
plugin_load=server_audit=server_audit.so
#plugin-load-add = auth_socket.so
character-set-server  = utf8
collation-server      = utf8_general_ci

max_connections = 2000

# InnoDB Configuration
default_storage_engine=innodb
innodb_autoinc_lock_mode=2
innodb_flush_log_at_trx_commit=0
innodb_buffer_pool_size=122M

# Galera Provider Configuration
wsrep_on=ON
wsrep_provider=/usr/lib/galera/libgalera_smm.so

# Galera Cluster Configuration
wsrep_provider_options="pc.recovery=TRUE;gcache.size=300M"
wsrep_cluster_name="openstack_cluster"
wsrep_cluster_address="gcomm://172.16.100.11:4567,172.16.100.12:4567,172.16.100.10:4567" ##podemos probar a usar nombers de host

# Galera Synchronization Configuration
wsrep_sst_method=rsync

# Galera Node Configuration
wsrep_node_address="172.16.100.1x"
wsrep_node_name="controller0x"

# * Logging and Replication
general_log_file        = /var/log/mysql/mysql.log
general_log             = 1
#
# Error log - should be very few entries.
#
log_error = /var/log/mysql/error.log
log-warnings = 2
#
#slow_query_log_file    = /var/log/mysql/mariadb-slow.log
#long_query_time = 10
#log_slow_rate_limit    = 1000
#log_slow_verbosity     = query_plan
#log-queries-not-using-indexes

```

>>>**NOTA:** cambiamos los parámetros `bind-address`, `wsrep_cluster_address` y `wsrep_node_name` acorde con el nodo que estamos configurando.

paramos el servicio en los controllers
```shell
service mysql stop
```

Inicializamos el primer nodo (controller01) y comprobamos que todo está bien:

```shell
galera_new_cluster
mysql -u root -p -e "SHOW STATUS LIKE 'wsrep_cluster_size'"
```

> +--------------------+-------+
> | Variable_name      | Value |
> +--------------------+-------+
> | wsrep_cluster_size | 1     |
> +--------------------+-------+

Iniciamos el resto de nodos con y comprobamos:

```shell
service mysql start
mysql -u root -p -e "SHOW STATUS LIKE 'wsrep_cluster_size'"
```

> +--------------------+-------+
> | Variable_name      | Value |
> +--------------------+-------+
> | wsrep_cluster_size | 2     |
> +--------------------+-------+

## Árbitro

Instalamos el árbitro en el balanceador:

```shell
apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
add-apt-repository 'deb [arch=amd64,i386,ppc64el] http://nyc2.mirrors.digitalocean.com/mariadb/repo/10.1/ubuntu xenial main'
apt update
apt install galera-arbitrator-3
```

y añadir esto al fichero `/etc/default/garb`:

```ini
   # Copyright (C) 2012 Codership Oy
   # This config file is to be sourced by garb service script.

   # A comma-separated list of node addresses (address[:port]) in the cluster
   GALERA_NODES="172.16.100.11:4567, 172.16.100.12:4567"

   # Galera cluster name, should be the same as on the rest of the nodes.
   GALERA_GROUP="openstack_cluster"

  # Optional Galera internal options string (e.g. SSL settings)
  # see http://galeracluster.com/documentation-webpages/galeraparameters.html
  # GALERA_OPTIONS=""

  # Log file for garbd. Optional, by default logs to syslog
  LOG_FILE="/var/log/garbd.log"


```

>>>**NOTA:** para ejecutar un árbitro y que éste arranque hay que hacer lo siguiente:

```shell
 touch /var/log/garbd.log && chown nobody /var/log/garbd.log
```

Arrancamos el servicio de árbitro:

```shell
systemctl enable garb
service garb start
```

Comprobamos que tenemos 3 miembros en el clúster, desde cualquier controller (controller01):

```shell
mysql -u root -p -e "SHOW STATUS LIKE 'wsrep_cluster_size'"
```

> +--------------------+-------+
> | Variable_name      | Value |
> +--------------------+-------+
> | wsrep_cluster_size | 3     |
> +--------------------+-------+

Creamos el usuario root para el resto de controllers (controller02,03...):

`mysql -u root -p`

```sql
create user 'root'@'controller0X' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON *.* TO 'root'@'controller0X' IDENTIFIED BY 'password' WITH GRANT OPTION;
FLUSH PRIVILEGES;
```

## DEBUGGING y EXTRAS

Para crear usuarios en la BBDD hay que usar **SIEMPRE** `create user XXX` y no `INSERT INTO ...`

```sql
SET GLOBAL server_audit_logging=OFF;
SET GLOBAL server_audit_events = 'CONNECT';
```

Para recuperar un cluster que no arranca, ejecutar esto en cada nodo y luego cambiar el valor `safe_to_bootstrap` del fichero `/var/lib/mysql/grastate.dat` a 1 del nodo con el número de secuencia más alto. En caso de que sea -1, elegir el que más rabia nos dé:

```shell
mysqld_safe --wsrep-recover
```