# COROSYNC Y PACEMAKER

## 1. Instalación
**Controller01 y Controller02 (en todos los controllers)**

Instalar los siguientes paquetes:

```shell
apt install pacemaker corosync pcs fence-agents resource-agents libqb0
```

## 2. Corosync

Habilitamos el servicio y lo añadimos al arranque:

```shell
systemctl default corosync
systemctl enable corosync
```

Movemos el fichero de configuración original:

```shell
mv /etc/corosync/corosync.conf /etc/corosync/corosync.conf.bak
```

Editamos el fichero /etc/corosync/corosync.conf`:

```json
totem {
    version: 2
    secauth: off
    cluster_name: openstackv1
    transport: udpu
}

nodelist {
    node {
        ring0_addr: controller01
        nodeid: 1
    }

    node {
        ring0_addr: controller02
        nodeid: 2
    }
}

quorum {
    provider: corosync_votequorum
    two_node: 1
}

logging {
    to_logfile: yes
    logfile: /var/log/corosync/corosync.log
    to_syslog: yes
}
```

**NOTA:** tal vez deberíamos añadir otra interfaz de red aquí, para que el anillo funcione mejor.

Ejecutamoms `mkdir -p /etc/corosync/uidgid.d/` y creamos el fichero `/etc/corosync/uidgid.d/pacemaker`:

```json
uidgid {
  uid: hacluster
  gid: haclient
}
```

## 3. Configuración

### IMPORTANTE: asegurarse de que en `/etc/hosts` NO hay referencias al nombre de la máquina con IPs locales, como por ejemplo:

```ini
127.0.1.1   controller01
::1     localhost ip6-localhost ip6-loopback
```

**Controller01 y Controller02 (en todos los controllers)**

Asegurarse de que `pcsd`  se inicia como servicio y establecer una contraseña al usuario `hacluster`:

```shell
passwd hacluster
systemctl enable pcsd
systemctl start pcsd

```

**En uno de los controllers**

Autenticamos todos los miembros del cluster:

```shell
pcs cluster auth controller01 controller02 -u hacluster -p CONTRASEÑA --force
```

>>> **NOTA:** controller01p y controller02p son las ip's públicas


Creamos y damos nombre al cluster, lo incializamos y habilitamos el autoarranque:

```shell
pcs cluster setup --force --name openstackv1 controller01 controller02
pcs cluster start --all
pcs cluster enable --all
```

En caso de que el comando `setup` nos hubiera devuelto un error, ejecutar `pcs cluster auth`, meter el usuario hacluster y la contraseña que hemos puesto antes y nos saldrá algo así:

>Username: hacluster

>Password:

>controller02p: Authorized

>controller01p: Authorized


## 4. Comprobación de estado

Podemos usar los siguientes comandos para ver el estado del cluster:

```shell
corosync-cmapctl | grep members
corosync-cfgtool -s
pcs status cluster
```

El resultado del último, si todo va bien sería algo así:

>Cluster Status:
> Last updated: Wed Apr 11 10:42:26 2018         Last change: Wed Apr 11 10:40:43 2018 by hacluster via crmd on controller01p
> Stack: corosync
> Current DC: controller01p (version 1.1.14-70404b0) - partition with quorum
> 2 nodes and 0 resources configured
> Online: [ controller01p controller02p ]
>PCSD Status:
> controller01p: Online
> controller02p: Online



## 5. Configuración de propiedades del cluster

**En uno de los controllers**

```shell
pcs property set pe-warn-series-max=1000 \
  pe-input-series-max=1000 \
  pe-error-series-max=1000 \
  cluster-recheck-interval=5min
```

En el entorno de desarrollo desactivaremos los mecanismos STONITH (apagar nodos vía IPMI o ssh cuando fallen), pero en producción hay que dejarlo activado, siempre y cuando tengamos mínimo 3 nodos. También desactivaremos el quorum, ya que con dos nodos no tiene sentido:

```shell
pcs property set stonith-enabled=false
pcs property set no-quorum-policy=ignore
```

## 5. Añadir IP de balanceo

Para añadir una Virtual IP escribimos lo siguiente:

```shell
 pcs resource create vip ocf:heartbeat:IPaddr2 \
  params ip="172.16.100.10" cidr_netmask="24" op monitor interval="30s"

   pcs resource create vip2 ocf:heartbeat:IPaddr2 \
  params ip="192.168.111.10" cidr_netmask="24" op monitor interval="30s"
```

>>> **NOTA:** podemos sustituir la IP 172.16.100.10 por 192.168.111.10 si queremos hacer el balanceo hacia fuera. Para borrar la VIP escribimos `pcs resource delete vip`
