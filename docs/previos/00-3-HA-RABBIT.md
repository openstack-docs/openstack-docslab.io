# Rabbit HA

## Crear un cluster de Rabbit

Parar rabbit en el nodo de destino y copiar la cookie desde el primer nodo

```shell
 rsync -av /var/lib/rabbitmq/.erlang.cookie root@controller02:/var/lib/rabbitmq/.erlang.cookie
```

En cada nodo de destino comprobar  que `.erlang.cookie` tiene los permisos y propietario correctos:

```shell
 chown rabbitmq:rabbitmq /var/lib/rabbitmq/.erlang.cookie
 chmod 400 /var/lib/rabbitmq/.erlang.cookie
```

Iniciamos el servicio de rabbit en la máquina destino (controller02) y ejecutamos los siguientes comandos:

```shell
service rabbitmq-server start
rabbitmqctl cluster_status
```

```shell
>Cluster status of node rabbit@controller02 ...
>[{nodes,[{disc,[rabbit@controller02]}]},
> {running_nodes,[rabbit@controller02]},
> {cluster_name,<<"rabbit@controller02">>},
> {partitions,[]}]
```

`rabbitmqctl stop_app`

```shell
> Stopping node rabbit@controller02 ...
```

`rabbitmqctl join_cluster rabbit@controller01`

```shell
>Clustering node rabbit@controller02 with rabbit@controller01 ...
>>> **NOTA:** Podemos cambiar el tipo de nodo si en vez de `--ram` ponemos `--disc`
```

`rabbitmqctl start_app`

```shell
>Starting node rabbit@controller02 ...
```

comprobamos que se ha unido al cluster (controller01 o 02 da lo mismo):

```shell
rabbitmqctl cluster_status
```

```shell
>Cluster status of node rabbit@controller01 ...
>[{nodes,[{disc,[rabbit@controller01,rabbit@controller02]}]},
> {running_nodes,[rabbit@controller02,rabbit@controller01]},
> {cluster_name,<<"rabbit@controller02">>},
> {partitions,[]}]
```

Para asegurarnos de que todas las colas excepto aquellas con nombres autogenerados se repliquen en todos los nodos, establecemos la política `ha-mode` ejecutando lo siguiente en uno de los nodos:

```shell
rabbitmqctl set_policy ha-all '^(?!amq\.).*' '{"ha-mode": "all"}'
```

## Configurar componentes de Openstack

En los ficheros de configuración de los componentes de Openstack, añadir lo siguiente:

```ini
transport_url = rabbit://openstackuser:openstackpass@controller01:5672,openstackuser:openstackpass@controller02:5672/openstack
rabbit_retry_interval=1
rabbit_retry_backoff=2
rabbit_max_retries=0
rabbit_durable_queues=true
rabbit_ha_queues=true
```

Y reiniciar los componentes

**Controller01 y Controller02:**
>/etc/cinder/cinder.conf
>/etc/heat/heat.conf
>/etc/ceilometer/ceilometer.conf
>/etc/nova/nova.conf
>/etc/neutron/neutron.conf
>/etc/glance/glance-api.conf
>/etc/glance/glance-registry.conf

**KVM01 y KVM02:**
>/etc/nova/nova.conf
>/etc/ceilometer/ceilometer.conf
>/etc/neutron/neutron.conf

**Gateway:**
>/etc/neutron/neutron.conf

**Storage:**
>/etc/cinder/cinder.conf